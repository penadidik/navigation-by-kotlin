package com.example.androidnavigationdrawerrecyclerview

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
//Created by 디딬 Didik M. Hadiningrat on 21 July 2019
import androidx.fragment.app.Fragment
import di2k.lintaspena.androidnavigation.R
import kotlinx.android.synthetic.main.fragment_layout.*


class AFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tv_label.text = "One"
    }
}
